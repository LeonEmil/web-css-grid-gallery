// Este es un ejemplo de como exportar funciones desde un archivo
// En index.js se importan estas funciones

const saludo = () => {
    console.log('Hola mundo')
};

const despedida = () => {
    console.log('Adiós mundo')
};

const hello = ():void => {
    console.log("Funciona en ES5!")
}


export {saludo, despedida, hello}